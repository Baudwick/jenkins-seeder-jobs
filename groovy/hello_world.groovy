def code = """
    pipeline {
        agent any
        stages {
            stage('Hello World') {
                steps {
                    echo "Hello World."
                }
            }
        }
        
    }
""";

pipelineJob('Hello World') {

    definition {
        cpsFlowDefinition {
            sandbox(true)
            script(code)
        }
    }
}